﻿CREATE TABLE [dbo].[Faces]
(
	[face_id] INT NOT NULL PRIMARY KEY, 
    [face_bmp] VARBINARY(MAX) NULL
)
