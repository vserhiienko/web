﻿CREATE TABLE [dbo].[People]
(
	[person_id] INT NOT NULL PRIMARY KEY, 
    [person_name] NVARCHAR(50) NULL, 
    [person_face_id] INT NULL, 
    [person_misc_info] NVARCHAR(MAX) NULL
)
