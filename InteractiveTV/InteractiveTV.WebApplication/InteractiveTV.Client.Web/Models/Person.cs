﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace InteractiveTV.Client.Web.Models
{
    public class Person
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string MiscInfo { get; set; }
        public string FaceBase64 { get; set; }

        public string Self
        {
            get
            {
                return string.Format(CultureInfo.CurrentCulture, "api/people/{0}", this.PersonId);
            }

            set
            {
            }
        }

    }
}