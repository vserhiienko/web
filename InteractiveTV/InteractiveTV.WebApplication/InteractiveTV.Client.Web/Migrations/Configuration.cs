namespace InteractiveTV.Client.Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    using InteractiveTV.Client.Web.Models;
    using System.IO;

    internal sealed class Configuration : DbMigrationsConfiguration<InteractiveTV.Client.Web.Models.PersonContext>
    {
        public Configuration()
        {

            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Base64Encode(plainTextBytes);
        }

        public static string Base64Encode(byte[] plainTextBytes)
        {
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        protected override void Seed(InteractiveTV.Client.Web.Models.PersonContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //if (System.Diagnostics.Debugger.IsAttached == false)
            //    System.Diagnostics.Debugger.Launch();

            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [People]");

            string picturesFolder = @"C:\Users\Vlad\Pictures\";

            string reeseBase64 = string.Empty;
            string evangelineBase64 = string.Empty;
            var jpgBytes = File.ReadAllBytes(picturesFolder + "reese-witherspoon_416x416.jpg");
            reeseBase64 = Base64Encode(jpgBytes);
            jpgBytes = File.ReadAllBytes(picturesFolder + "Evangeline-Lilly-The-Hobbit-Desolation-of-Smaug.jpg");
            evangelineBase64 = Base64Encode(jpgBytes);

            context.People.AddOrUpdate(
                person => person.Name,
                //new Person { Name = "Vlad Serhiienko", MiscInfo = "Admin", FaceBase64 = "" },
                new Person { Name = "Laura Jeanne Reese Witherspoon", MiscInfo = "American actress and produce", FaceBase64 = reeseBase64 },
                new Person { Name = "Nicole Evangeline Lilly", MiscInfo = "Canadian actress and author", FaceBase64 = evangelineBase64 }
                );

            //context.SaveChanges();

        }
    }
}
