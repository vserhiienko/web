﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveTV.Client
{
    class Program
    {
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Base64Encode(plainTextBytes);
        }

        public static string Base64Encode(byte[] plainTextBytes)
        {
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        static void Main(string[] args)
        {
            string picturesFolder = @"C:\Users\Vlad\Pictures\";

            string reeseBase64 = string.Empty;
            string evangelineBase64 = string.Empty;
            var jpgBytes = File.ReadAllBytes(picturesFolder + "reese-witherspoon_416x416.jpg");
            reeseBase64 = Base64Encode(jpgBytes);
            jpgBytes = File.ReadAllBytes(picturesFolder + "Evangeline-Lilly-The-Hobbit-Desolation-of-Smaug.jpg");
            evangelineBase64 = Base64Encode(jpgBytes);
        }
    }
}
