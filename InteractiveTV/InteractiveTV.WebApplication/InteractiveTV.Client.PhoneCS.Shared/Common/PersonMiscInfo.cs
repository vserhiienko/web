﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Data.Json;

namespace InteractiveTV.Client
{
    public class PersonMiscInfo
    {
        public string UserInfo { get; set; }
        public string ImageFormat { get; set; }
        public int ImagePixelWidth { get; set; }
        public int ImagePixelHeight { get; set; }

        public string ToJson()
        {
            JsonObject jsonObject = new JsonObject
            {
                {
                    "UserInfo", JsonValue.CreateStringValue(UserInfo)
                },
                {
                    "ImageFormat", JsonValue.CreateStringValue(ImageFormat)
                },
                {
                    "ImagePixelWidth", JsonValue.CreateStringValue(ImagePixelWidth.ToString())
                },
                {
                    "ImagePixelHeight", JsonValue.CreateStringValue(ImagePixelHeight.ToString())
                },
            };

            return jsonObject.Stringify();
        }
    }
}
