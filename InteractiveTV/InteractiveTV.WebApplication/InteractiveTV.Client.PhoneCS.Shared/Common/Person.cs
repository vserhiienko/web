﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage;

namespace InteractiveTV.Client
{
    public class Person
    {
        public string Name { get; set; }
        public string MiscInfo { get; set; }
        public string FaceBase64 { get; set; }

        static readonly string InteractiveTVPeopleApi = "http://interactivetvclientweb.azurewebsites.net/api/people/";


        public HttpWebRequest NewPostRequest
        {
            get
            {
                var http = HttpWebRequest.CreateHttp(InteractiveTVPeopleApi);
                http.ContentType = "application/x-www-form-urlencoded";
                http.Method = "POST";
                return http;
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Base64Encode(plainTextBytes);
        }

        public static string Base64Encode(byte[] plainTextBytes)
        {
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes, 0, base64EncodedBytes.Length);
        }

        public async Task<bool> SetFaceBase64From(string fileName)
        {
            try
            {
                var jpegStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("Selfie.jpg");

                var bytes = default(byte[]);
                using (var memstream = new MemoryStream())
                {
                    var buffer = new byte[512];
                    var bytesRead = default(int);

                    while ((bytesRead = jpegStream.Read(buffer, 0, buffer.Length)) > 0)
                        memstream.Write(buffer, 0, bytesRead);
                    bytes = memstream.ToArray();
                }

                FaceBase64 = Base64Encode(bytes);
                return true;
            }
            catch (Exception e)
            {

                return false;
            }
        }

        public async Task<string> PostToDatabaseAsync()
        {
            var postRequest = NewPostRequest;
            using (var streamWriter = new System.IO.StreamWriter(await postRequest.GetRequestStreamAsync()))
            {
                streamWriter.Write("Name=" + Name);
                streamWriter.Write("&MiscInfo=" + MiscInfo);
                streamWriter.Write("&FaceBase64=" + FaceBase64);
            }

            var responseToPost = await postRequest.GetResponseAsync();
            var responseStream = responseToPost.GetResponseStream();

            string responseText = String.Empty;
            using (var streamReader = new System.IO.StreamReader(responseStream, Encoding.UTF8))
            {
                var readBuffer = new Char[256];
                int readByteCount = streamReader.Read(readBuffer, 0, 256);

                while (readByteCount > 0)
                {
                    String partialResponseText = new String(readBuffer, 0, readByteCount);
                    responseText += partialResponseText;
                    readByteCount = streamReader.Read(readBuffer, 0, 256);
                }
            }

            return responseText;
        }
    }
}
