﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Lumia;
using Lumia.Imaging;
using Windows.UI.Core;
using Windows.Storage.Streams;
using InteractiveTV.Client;
using Windows.UI.Popups;
using Windows.Graphics.Display;
using Windows.ApplicationModel.Core;
using Windows.Storage.Pickers;
using Windows.ApplicationModel.Activation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace InteractiveTV.Client.PhoneCS
{
    public class UniversalCamera
    {
        private static UniversalCamera _Default;
        public static UniversalCamera GetDefault()
        {
            if (_Default == null)
            {
                _Default = new UniversalCamera();
            }
            return _Default;
        }

        MediaCapture captureManager;
        public CaptureElement capturePreview;
        ImageEncodingProperties imageProperties = ImageEncodingProperties.CreateJpeg();

        public async System.Threading.Tasks.Task InitPreview(CaptureElement capturePreview)
        {
            this.capturePreview = capturePreview;
            this.captureManager = new MediaCapture();

            MediaCaptureInitializationSettings settings = new Windows.Media.Capture.MediaCaptureInitializationSettings();
            settings.StreamingCaptureMode = StreamingCaptureMode.Video;
            await this.captureManager.InitializeAsync(settings);
            this.capturePreview.Source = captureManager;
            await this.captureManager.StartPreviewAsync();
        }

        private static DeviceInformation GetCameraID(Windows.Devices.Enumeration.Panel desiredCamera)
        {
            var findCameraDeviceCollectionTask = DeviceInformation.FindAllAsync(DeviceClass.VideoCapture).AsTask();
            findCameraDeviceCollectionTask.Wait();
            var findCameraDeviceCollection = findCameraDeviceCollectionTask.Result;

            return findCameraDeviceCollection.FirstOrDefault(
                x => x.EnclosureLocation != null &&
                x.EnclosureLocation.Panel == desiredCamera
                );
        }

        async public void InitCamera()
        {
            var cameraDevice = GetCameraID(Windows.Devices.Enumeration.Panel.Front);

            captureManager = new MediaCapture();
            captureManager.InitializeAsync(new MediaCaptureInitializationSettings
                {
                    StreamingCaptureMode = StreamingCaptureMode.Video,
                    //PhotoCaptureSource = PhotoCaptureSource.Photo,
                    PhotoCaptureSource = PhotoCaptureSource.VideoPreview,
                    AudioDeviceId = string.Empty,
                    VideoDeviceId = cameraDevice.Id
                })
                .AsTask()
                .Wait();

            var maxResolution = captureManager
                .VideoDeviceController
                .GetAvailableMediaStreamProperties(MediaStreamType.Photo)
                .Aggregate((w, h) => (w as VideoEncodingProperties).Width > (h as VideoEncodingProperties).Width ? w : h);

            captureManager
                .VideoDeviceController
                .SetMediaStreamPropertiesAsync(MediaStreamType.Photo, maxResolution)
                .AsTask()
                .Wait();
        }

        async public void StartPreview()
        {
            // rotate to see preview vertically
            //captureManager.SetPreviewRotation(VideoRotation.Clockwise90Degrees);
            capturePreview.Source = captureManager;
            await captureManager.StartPreviewAsync();
        }

        async public void StopPreview()
        {
            if (captureManager != null)
            {
                captureManager.StopPreviewAsync();
                captureManager.Dispose();
                captureManager = null;
            }
        }

        public IStorageFile TakePhoto()
        {
            InMemoryRandomAccessStream memStream = new InMemoryRandomAccessStream();
            var photoFileTask = ApplicationData.Current.LocalFolder.CreateFileAsync("Selfie.jpg", CreationCollisionOption.ReplaceExisting).AsTask();

            photoFileTask.Wait();
            var photoFile = photoFileTask.Result;
 
            captureManager.CapturePhotoToStreamAsync(
                ImageEncodingProperties.CreateUncompressed(MediaPixelFormat.Bgra8), 
                memStream
                ).AsTask().Wait();
            captureManager.CapturePhotoToStorageFileAsync(
                ImageEncodingProperties.CreateJpeg(),
                photoFile
                ).AsTask().Wait();

            return photoFile;
            //return (ImageSource)new BitmapImage(
            //    new Uri(photoFile.Path)
            //    );
            //return new Task<ImageSource>(() => (ImageSource)new BitmapImage(
            //    new Uri(photoFile.Path)
            //    ));
        }
    }


    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private bool ignoreCameraButton;
        private UniversalCamera camera;
        string capturedImagePath;

        public MainPage()
        {
            ignoreCameraButton = false;

            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;

            Loaded += OnMainPageLoaded;
            Unloaded += OnMainPageUnloaded;

            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
        }

        private void OnMainPageUnloaded(object sender, RoutedEventArgs e)
        {
            ShutdownCamera();
        }

        private void OnMainPageLoaded(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void ShutdownCamera()
        {
            if (camera != null)
            {
                camera.StopPreview();
                camera.capturePreview = null;
                camera = null;

                openCameraButton.IsChecked = false;
                capturePreview.Visibility = Visibility.Collapsed;
                takePhotoButton.Visibility = Visibility.Collapsed;
                tooltipTextBlock.Visibility = Visibility.Visible;
            }
        }

        private void OpenCamera()
        {
            if (camera == null && !ignoreCameraButton)
            {
                ignoreCameraButton = true;
                capturePreview.Visibility = Visibility.Visible;
                imageContainer.Visibility = Visibility.Collapsed;

                camera = UniversalCamera.GetDefault();
                camera.capturePreview = capturePreview;
                    
                Task.Run(() =>
                {
                    camera.InitCamera();
                    this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, new DispatchedHandler(() =>
                    {
                        camera.StartPreview();

                        ignoreCameraButton = false;
                        takePhotoButton.Visibility = Visibility.Visible;
                        tooltipTextBlock.Visibility = Visibility.Collapsed;
                    }));
                });
            }
        }

        private void CameraAppBarButtonClicked(object sender, RoutedEventArgs e)
        {
            if (camera == null) 
                OpenCamera();
            else 
                ShutdownCamera();
        }

        string CreateRandomString()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(
                Enumerable.Repeat(chars, random.Next(16, 32))
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray()
                          );
        }

        private void FileAppBarButtonClicked(object sender, RoutedEventArgs e)
        {
            imageContainer.Visibility = Visibility.Collapsed;
            ShutdownCamera();

            if (capturedImagePath == null || capturedImagePath == string.Empty)
            {
                CoreApplicationView view = CoreApplication.GetCurrentView();

                imageFilePath = string.Empty;
                FileOpenPicker filePicker = new FileOpenPicker();
                filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                filePicker.ViewMode = PickerViewMode.Thumbnail;

                // Filter to include a sample subset of file types
                filePicker.FileTypeFilter.Clear();
                filePicker.FileTypeFilter.Add(".bmp");
                filePicker.FileTypeFilter.Add(".png");
                filePicker.FileTypeFilter.Add(".jpeg");
                filePicker.FileTypeFilter.Add(".jpg");

                try
                {
                    filePicker.PickSingleFileAndContinue();
                    view.Activated += FilePickerViewActivated;
                }
                catch (Exception ex)
                {
                    new MessageDialog(ex.Message, "Error").ShowAsync();
                }
            }
            else
            {
                try
                {
                    if (capturedImagePath != null && capturedImagePath != string.Empty)
                        Frame.Navigate(typeof(SendPersonPage), capturedImagePath);
                }
                catch (Exception ex)
                {
                    new MessageDialog(ex.Message, "Error").ShowAsync();
                }
            }
        }

        private void FilePickerViewActivated(CoreApplicationView sender, Windows.ApplicationModel.Activation.IActivatedEventArgs args)
        {
            FileOpenPickerContinuationEventArgs pickerContinuationEventArgs = args as FileOpenPickerContinuationEventArgs;

            if (pickerContinuationEventArgs != null)
            {
                if (pickerContinuationEventArgs.Files.Count == 0) return;

                sender.Activated -= FilePickerViewActivated;
                capturedImagePath = pickerContinuationEventArgs.Files[0].Path;
                FileAppBarButtonClicked(this, null);
            }
        }

        private async void TakePhotoAppBarButtonClicked(object sender, RoutedEventArgs e)
        {
            if (camera != null)
            {
                Task.Run(() =>
                {
                    var captureImageFile = camera.TakePhoto();
                    capturedImagePath = captureImageFile.Path;
                    this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, new DispatchedHandler(() =>
                        {
                            ShutdownCamera();

                            imageContainer.Source = new BitmapImage(new Uri(captureImageFile.Path));
                            imageContainer.Visibility = Visibility.Visible;

                        }));
                });
            }
        }

        public string imageFilePath { get; set; }
    }
}
