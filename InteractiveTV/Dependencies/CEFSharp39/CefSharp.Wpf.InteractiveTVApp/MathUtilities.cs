﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharp.Wpf.InteractiveTVApp
{
    public static class MathUtilities
    {
        static public float CalculateDistanceTo(this PXCMPointF32 p0, PXCMPointF32 p1)
        {
            double dx = (p0.x - p1.x);
            double dy = (p0.y - p1.y);
            return (float)Math.Sqrt(dx * dx + dy * dy);
        }
        static public float CalculateDistanceSquaredTo(this PXCMPointF32 p0, PXCMPointF32 p1)
        {
            var dx = (p0.x - p1.x);
            var dy = (p0.y - p1.y);
            return dx * dx + dy * dy;
        }
    }
}
