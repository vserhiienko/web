﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CefSharp.Wpf.InteractiveTVApp
{
   /// <summary>
   /// Interaction logic for App.xaml
   /// </summary>
   public partial class App : Application
   {
      public static LazyDisposable<PXCMSession> PXCMSessionInstance = new LazyDisposable<PXCMSession>(() =>
      {
         PXCMSession session = null;
         pxcmStatus sts = PXCMSession.CreateInstance(out session);
         if (sts >= pxcmStatus.PXCM_STATUS_NO_ERROR)
         {
            Debug.WriteLine("App.PXCMSessionInstance.LazyGet: PXC Initialized");
            return session;
         }
         else
         {
            if (session != null)
               session.Dispose();
            Debug.WriteLine("App.PXCMSessionInstance.LazyGet: Failed to initialize PXC");
            //throw new Exception("Failed to initialize PXC");
            return null;
         }
      }, true);

      // Use when debugging the actual SubProcess, to make breakpoints etc. inside that project work.
      private static readonly bool DebuggingSubProcess = Debugger.IsAttached;

      private App()
      {
         var settings = new CefSettings();
         settings.RemoteDebuggingPort = 8088;
         //The location where cache data will be stored on disk. If empty an in-memory cache will be used for some features and a temporary disk cache for others.
         //HTML5 databases such as localStorage will only persist across sessions if a cache path is specified. 
         settings.CachePath = "cache";
         settings.LogSeverity = LogSeverity.Verbose;


         Cef.OnContextInitialized = delegate
         {
            Cef.SetCookiePath("cookies", true);
         };

         if (!Cef.Initialize(settings, shutdownOnProcessExit: true, performDependencyCheck: !DebuggingSubProcess))
         {
            Debug.WriteLine("App.App: Failed to initialize CEF");
            throw new Exception("Failed to initialize CEF");
         }
         else
         {
            Debug.WriteLine("App.App: CEF Initialized");
         }

         //if (PXCMSessionInstance.Value == null)
         //{
         //   Debug.WriteLine("App.App: Failed to initialize PXC");
         //}
      }
   }
}
