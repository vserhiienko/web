﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharp.Wpf.InteractiveTVApp
{
    public class LazySingleton<T> where T : class, new()
    {
        private static Lazy<T> _instance = new Lazy<T>();
        public static T Instance { get { return _instance.Value; } }
    }
}
