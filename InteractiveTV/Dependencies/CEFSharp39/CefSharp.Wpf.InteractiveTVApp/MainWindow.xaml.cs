﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CefSharp.Wpf.InteractiveTVApp
{
   public class ClickOnPointTool
   {

      [DllImport("user32.dll")]
      static extern bool ClientToScreen(IntPtr hWnd, ref Point lpPoint);

      [DllImport("user32.dll")]
      internal static extern uint SendInput(uint nInputs, [MarshalAs(UnmanagedType.LPArray), In] INPUT[] pInputs, int cbSize);

#pragma warning disable 649
      internal struct INPUT
      {
         public UInt32 Type;
         public MOUSEKEYBDHARDWAREINPUT Data;
      }

      [StructLayout(LayoutKind.Explicit)]
      internal struct MOUSEKEYBDHARDWAREINPUT
      {
         [FieldOffset(0)]
         public MOUSEINPUT Mouse;
      }

      internal struct MOUSEINPUT
      {
         public Int32 X;
         public Int32 Y;
         public UInt32 MouseData;
         public UInt32 Flags;
         public UInt32 Time;
         public IntPtr ExtraInfo;
      }

#pragma warning restore 649

      public static void ToScreen(IntPtr h, int x, int y, out int xx, out int yy)
      {
         Point p = new Point(x, y);
         ClientToScreen(h, ref p);
         xx = (int)p.X;
         yy = (int)p.Y;
      }


      // 1 move
      // 2 down
      // 4 up
      public static void ClickOnPoint(IntPtr wndHandle, Point clientPoint, uint flags)
      {
         ClientToScreen(wndHandle, ref clientPoint);

         var inputMouseDown = new INPUT();
         inputMouseDown.Type = 0; /// input type mouse
         inputMouseDown.Data.Mouse.Flags = flags; /// left button down
         inputMouseDown.Data.Mouse.X = (int)clientPoint.X;
         inputMouseDown.Data.Mouse.Y = (int)clientPoint.X;

         var inputs = new INPUT[] { inputMouseDown };
         //var inputs = new INPUT[] { inputMouseDown, inputMouseUp };
         SendInput((uint)inputs.Length, inputs, Marshal.SizeOf(typeof(INPUT)));
      }

   }



   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      const int SW_HIDE = 0;
      const int SW_SHOW = 5;

      private const int WM_MOUSEMOVE = 0x200;
      private const int WM_MOUSEDOWN = 0x201;
      private const int WM_MOUSEUP = 0x202;
      const int MK_LBUTTON = 0x0001;

      [DllImport("user32.dll")]
      private static extern IntPtr GetForegroundWindow();
      [DllImport("kernel32.dll", SetLastError = true)]
      static extern bool AllocConsole();
      [DllImport("kernel32.dll")]
      static extern IntPtr GetConsoleWindow();
      [DllImport("user32.dll")]
      static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
      [DllImport("user32.dll")]
      private static extern int SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

      public enum TOUCH_MASK : uint
      {
         TOUCH_MASK_NONE = 0x00000000,
         TOUCH_MASK_CONTACTAREA = 0x00000001,
         TOUCH_MASK_ORIENTATION = 0x00000002,
         TOUCH_MASK_PRESSURE = 0x00000004
      }
      public enum POINTER_INPUT_TYPE : uint
      {
         PT_POINTER = 0x00000001,
         PT_TOUCH = 0x00000002,
         PT_PEN = 0x00000003,
         PT_MOUSE = 0x00000004
      }

      public enum POINTER_FLAGS : uint
      {
         POINTER_FLAG_NONE = 0x00000000,
         POINTER_FLAG_NEW = 0x00000001,
         POINTER_FLAG_INRANGE = 0x00000002,
         POINTER_FLAG_INCONTACT = 0x00000004,
         POINTER_FLAG_FIRSTBUTTON = 0x00000010,
         POINTER_FLAG_SECONDBUTTON = 0x00000020,
         POINTER_FLAG_THIRDBUTTON = 0x00000040,
         POINTER_FLAG_OTHERBUTTON = 0x00000080,
         POINTER_FLAG_PRIMARY = 0x00000100,
         POINTER_FLAG_CONFIDENCE = 0x00000200,
         POINTER_FLAG_CANCELLED = 0x00000400,
         POINTER_FLAG_DOWN = 0x00010000,
         POINTER_FLAG_UPDATE = 0x00020000,
         POINTER_FLAG_UP = 0x00040000,
         POINTER_FLAG_WHEEL = 0x00080000,
         POINTER_FLAG_HWHEEL = 0x00100000
      }
      public enum TOUCH_FEEDBACK : uint
      {
         TOUCH_FEEDBACK_DEFAULT = 0x1,
         TOUCH_FEEDBACK_INDIRECT = 0x2,
         TOUCH_FEEDBACK_NONE = 0x3
      }

      [DllImport("TouchInjectionDriver.dll", CallingConvention = CallingConvention.Cdecl)]
      private static extern bool InjectTouch(int x, int y, POINTER_INPUT_TYPE pt_input, int pressure, int orientation, int id, int rcContactTop, int rcContactBottom, int rcContactLeft, int rcContactRight, POINTER_FLAGS pointerFlags, TOUCH_MASK touchMask);
      [DllImport("TouchInjectionDriver.dll", CallingConvention = CallingConvention.Cdecl)]
      private static extern void setTouchFeedback(TOUCH_FEEDBACK fb);
      [DllImport("TouchInjectionDriver.dll", CallingConvention = CallingConvention.Cdecl)]
      private static extern void setDefaultRectSize(int size);
      [DllImport("TouchInjectionDriver.dll", CallingConvention = CallingConvention.Cdecl)]
      private static extern void setDefaultPressure(int pres);
      [DllImport("TouchInjectionDriver.dll", CallingConvention = CallingConvention.Cdecl)]
      private static extern void setDefaultOrientation(int or);

      [DllImport("User32.dll")]
      static extern Boolean MessageBeep(UInt32 beepType);

      public static void TouchMouseDown(IntPtr h, int xx, int yy)
      {
         int x, y;
         ClickOnPointTool.ToScreen(h, xx, yy, out x, out y);

         bool ret;
         setTouchFeedback(TOUCH_FEEDBACK.TOUCH_FEEDBACK_INDIRECT);
         ret = InjectTouch(x, y, POINTER_INPUT_TYPE.PT_TOUCH, 3200, 0, 0, x - 4, x + 4, y - 4, y + 4, POINTER_FLAGS.POINTER_FLAG_DOWN | POINTER_FLAGS.POINTER_FLAG_INCONTACT | POINTER_FLAGS.POINTER_FLAG_INRANGE, TOUCH_MASK.TOUCH_MASK_CONTACTAREA | TOUCH_MASK.TOUCH_MASK_ORIENTATION | TOUCH_MASK.TOUCH_MASK_PRESSURE);

         if (ret)
            MessageBeep(0);
      }

      public static void TouchMouseUpdate(IntPtr h, int xx, int yy)
      {
         bool ret;
         int x, y;
         ClickOnPointTool.ToScreen(h, xx, yy, out x, out y);

         ret = InjectTouch(x, y, POINTER_INPUT_TYPE.PT_TOUCH, 3200, 0, 0, x - 4, x + 4, y - 4, y + 4, POINTER_FLAGS.POINTER_FLAG_UPDATE, TOUCH_MASK.TOUCH_MASK_CONTACTAREA | TOUCH_MASK.TOUCH_MASK_ORIENTATION | TOUCH_MASK.TOUCH_MASK_PRESSURE);
         if (!ret)
            MessageBeep(0);
      }

      public static void TouchMouseUp(IntPtr h, int xx, int yy)
      {
         bool ret;
         int x, y;
         ClickOnPointTool.ToScreen(h, xx, yy, out x, out y);

         ret = InjectTouch(x, y, POINTER_INPUT_TYPE.PT_TOUCH, 3200, 0, 0, x - 4, x + 4, y - 4, y + 4, POINTER_FLAGS.POINTER_FLAG_UP, TOUCH_MASK.TOUCH_MASK_CONTACTAREA | TOUCH_MASK.TOUCH_MASK_ORIENTATION | TOUCH_MASK.TOUCH_MASK_PRESSURE);
         if (!ret)
            MessageBeep(0);
      }

      static void SendMessage(IntPtr handle, uint msg, int X, int Y)
      {
         int x, y;
         ClickOnPointTool.ToScreen(handle, X, Y, out x, out y);

         Debug.WriteLine("SendMessage: screen " + x +  " " + y + "; client " +  X + " " + Y + ";");

         IntPtr lParam = (IntPtr)((y << 16) | x);
         IntPtr wParam = IntPtr.Zero;
         SendMessage(handle, msg, wParam, lParam);
      }

      bool sensorStop;
      bool cursorIsHere;
      bool cursorIsHidden;
      bool sensorGridVisible;
      bool trackingHand;

      Task sensorTask;
      InteractiveTVSensor sensor;
      System.Windows.Forms.Timer sensorTimer;
      CancellationTokenSource sensorTokenSource;

      BitmapImage[] images;
      int handPositionIndex;
      PXCMPointF32 starDragPosition;
      PXCMPointF32 prevHandPosition;
      List<PXCMPointF32> handPositions;

      NLua.Lua luaContext;
      NLua.LuaFunction luaOnPCXFaces;
      NLua.LuaFunction luaOnPCXHand;
      NLua.LuaFunction luaOnKeyUp;
      NLua.LuaFunction luaOnBrowserLoaded;
      NLua.LuaFunction luaOnBrowserSetAddress;
      NLua.LuaFunction luaOnPCXGesture;
      NLua.LuaFunction luaOnPCXInitialized;

      public ChromiumWebBrowser Browser { get; set; }

      static readonly int SmoothingStrength = 16;
      static readonly float SmoothingStrengthF = SmoothingStrength;
      //private IntPtr windowHandle;

      public void Lua_AllocateOrShowConsoleWindow()
      {
         var consoleHandle = GetConsoleWindow();
         if (consoleHandle == IntPtr.Zero) AllocConsole();
         else ShowWindow(consoleHandle, SW_SHOW);
      }

      public IntPtr WindowHandle
      {
         get
         {
            //return GetForegroundWindow();
            return new WindowInteropHelper(this).Handle;
         }

      }


      public MainWindow()
      {

         InitializeComponent();
         luaContext = new NLua.Lua();
         luaContext.LoadCLRPackage();
         luaContext.DoString("import ('CefSharp.Wpf.InteractiveTVApp.exe', 'CefSharp.Wpf.InteractiveTVApp')");
         luaContext.DoString("import ('System')");
         luaContext.DoString("import ('System.Diagnostics')");

         luaContext["mainWindow"] = this;
         luaContext["gestureLabel_SwipeLeft"] = PXCMGesture.Gesture.Label.LABEL_NAV_SWIPE_LEFT.ToString();
         luaContext["gestureLabel_SwipeRight"] = PXCMGesture.Gesture.Label.LABEL_NAV_SWIPE_RIGHT.ToString();
         luaContext["gestureLabel_SwipeUp"] = PXCMGesture.Gesture.Label.LABEL_NAV_SWIPE_UP.ToString();
         luaContext["gestureLabel_SwipeDown"] = PXCMGesture.Gesture.Label.LABEL_NAV_SWIPE_DOWN.ToString();
         luaContext["gestureLabel_Circle"] = PXCMGesture.Gesture.Label.LABEL_HAND_CIRCLE.ToString();
         luaContext["gestureLabel_Wave"] = PXCMGesture.Gesture.Label.LABEL_HAND_WAVE.ToString();
         luaContext["gestureLabel_PosePeace"] = PXCMGesture.Gesture.Label.LABEL_POSE_PEACE.ToString();
         luaContext["gestureLabel_PosePalm"] = PXCMGesture.Gesture.Label.LABEL_POSE_BIG5.ToString();

         luaContext.DoFile(GetType().Assembly.GetName().Name + ".Script.lua");
         luaOnPCXInitialized = luaContext["Lua_OnPXCInitialized"] as NLua.LuaFunction;
         luaOnPCXFaces = luaContext["Lua_OnPXCFaces"] as NLua.LuaFunction;
         luaOnPCXGesture = luaContext["Lua_OnPXCGesture"] as NLua.LuaFunction;
         luaOnBrowserLoaded = luaContext["Lua_OnBrowserLoaded"] as NLua.LuaFunction;
         luaOnBrowserSetAddress = luaContext["Lua_OnBrowserSetAddress"] as NLua.LuaFunction;
         luaOnKeyUp = luaContext["Lua_OnKeyUp"] as NLua.LuaFunction;

#if !DEBUG
         if (luaOnPCXInitialized != null)
            luaOnPCXInitialized.Call(App.PXCMSessionInstance.Value != null, true);
#else
         if (luaOnPCXInitialized != null)
            luaOnPCXInitialized.Call(App.PXCMSessionInstance.Value != null, false);
#endif

         Debug.WriteLine("MainWindow.MainWindow");
         sensorStop = false;

         Browser = new ChromiumWebBrowser();
         Browser.Loaded += OnBrowserLoaded;
         Browser.Address = luaOnBrowserSetAddress.Call()[0].ToString();

         //Browser.MouseLeftButtonDown += Browser_MouseLeftButtonDown;
         //Browser.MouseLeftButtonUp += Browser_MouseLeftButtonUp;
         //Browser.MouseMove += Browser_MouseMove;

         Browser.ConsoleMessage += Browser_ConsoleMessage;
         //windowHandle = new WindowInteropHelper(this).Handle;

         rootGrid.Children.Add(Browser);
         Grid.SetRow(Browser, 1);
         Grid.SetColumn(Browser, 1);

         cursorIsHere = false;
         trackingHand = false;

#if DEBUG
         cursorIsHidden = false;
         sensorGridVisible = true;
         sensorGrid.Visibility = System.Windows.Visibility.Visible;
#else
         cursorIsHidden = false;
         sensorGridVisible = false;
         sensorGrid.Visibility = System.Windows.Visibility.Collapsed;
#endif
         sensorTimer = new System.Windows.Forms.Timer();
         sensorTimer.Interval = 10000;
         sensorTimer.Tick += OnSensorTimerTick;
         sensorTimer.Start();

         images = new BitmapImage[2];

         handPositionIndex = 0;
         handPositions = new List<PXCMPointF32>(SmoothingStrength);
         foreach (var item in Enumerable.Range(0, SmoothingStrength))
            handPositions.Add(new PXCMPointF32());

         this.KeyUp += OnKeyUp;
         this.Closing += OnClosing;
         //this.MouseEnter += OnMouseEnter;
         //this.MouseLeave += OnMouseLeave;
         //this.MouseMove += MainWindow_MouseMove;
      }

      private void Browser_ConsoleMessage(object sender, ConsoleMessageEventArgs e)
      {
         Debug.WriteLine(e.Message);

      }

      private void Browser_MouseMove(object sender, MouseEventArgs e)
      {
         Debug.WriteLine("Browser_MouseMove");
      }

      private void Browser_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
      {
         Debug.WriteLine("Browser_MouseLeftButtonUp");
      }

      private void Browser_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
         Debug.WriteLine("Browser_MouseLeftButtonDown");
      }

      private void MainWindow_MouseMove(object sender, MouseEventArgs e)
      {
         e.Handled = true;
      }

      private void AddHandPosition(double x, double y)
      {
         PXCMPointF32 p;
         p.x = (float)x;
         p.y = (float)y;
         handPositionIndex = (handPositionIndex + 1) % SmoothingStrength;
         handPositions[handPositionIndex] = p;
      }

      private PXCMPointF32 AvgHandPosition()
      {
         PXCMPointF32 avg = new PXCMPointF32();
         foreach (var i in Enumerable.Range(0, SmoothingStrength))
         {
            avg.x += handPositions[i].x;
            avg.y += handPositions[i].y;
         }
         avg.x /= SmoothingStrengthF;
         avg.y /= SmoothingStrengthF;
         return avg;
      }

      private void OnSensorTimerTick(object sender, EventArgs e)
      {
         DiscardActivities();
      }

      private static void TimerCallback(object state)
      {
         if (state != null)
         {
            var self = (MainWindow)state;
            self.DiscardActivities();
         }
      }

      private void ResumeActivities()
      {
         sensorTimer.Stop();
         sensorTimer.Start();
      }

      private void DiscardActivities()
      {
         Debug.WriteLine("DiscardActivities");
      }

      private void OnMouseLeave(object sender, MouseEventArgs e)
      {
         cursorIsHere = false;
         if (cursorIsHidden)
            Mouse.OverrideCursor = Cursors.Arrow;
      }

      private void OnMouseEnter(object sender, MouseEventArgs e)
      {
         cursorIsHere = true;
         if (cursorIsHidden)
            Mouse.OverrideCursor = Cursors.None;
      }

      private async void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
      {
         sensorStop = true;
         sensorTokenSource.Cancel();
         await sensorTask;
      }

      private void SwitchToNormalState()
      {
         if (this.WindowState == System.Windows.WindowState.Normal) return;
         Debug.WriteLine("MainWindow.SwitchToNormalState");
         this.WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
         this.WindowState = System.Windows.WindowState.Normal;
      }

      private void SwitchToFullscreenState()
      {
         if (this.WindowState == System.Windows.WindowState.Maximized) return;
         Debug.WriteLine("MainWindow.SwitchToFullscreenState");
         this.WindowStyle = System.Windows.WindowStyle.None;
         this.WindowState = System.Windows.WindowState.Maximized;
      }

      private void OnKeyUp(object sender, KeyEventArgs e)
      {
         switch (e.Key)
         {
            case Key.Escape:
               switch (this.WindowState)
               {
                  case WindowState.Maximized: SwitchToNormalState(); break;
                  case WindowState.Normal: Close(); break;
               }
               break;
            case Key.F3:
               if (cursorIsHere)
               {
                  if (cursorIsHidden = !cursorIsHidden)
                     Mouse.OverrideCursor = Cursors.None;
                  else
                     Mouse.OverrideCursor = Cursors.Arrow;
               }
               break;
            case Key.F2:
               if (sensorGridVisible)
               {
                  sensorGridVisible = false;
                  sensorGrid.Visibility = System.Windows.Visibility.Collapsed;
               }
               else
               {
                  sensorGridVisible = true;
                  sensorGrid.Visibility = System.Windows.Visibility.Visible;
               }
               break;
            case Key.F1:
               switch (this.WindowState)
               {
                  case WindowState.Maximized: SwitchToNormalState(); break;
                  case WindowState.Normal: SwitchToFullscreenState(); break;
               }
               break;

            default:
               if (luaOnKeyUp != null)
                  luaOnKeyUp.Call(
                     e.Key, e.SystemKey,
                     e.Key.ToString(), e.SystemKey.ToString()
                     );
               break;
         }
      }

      private void OnBrowserLoaded(object sender, RoutedEventArgs e)
      {
         Dispatcher.Invoke(() =>
         {
            Debug.WriteLine("MainWindow.OnBrowserLoaded");
            luaOnBrowserLoaded.Call();
#if !DEBUG
            SwitchToFullscreenState();
#endif
            if (App.PXCMSessionInstance.Value != null)
            {
               sensorTokenSource = new CancellationTokenSource();
               sensorTask = Task.Run(() =>
               {
                  try
                  {
                     Debug.WriteLine("MainWindow.OnBrowserLoaded: Starting sensor");
                     sensor = new InteractiveTVSensor();
                     sensor.SimplePipeline(
                              SensorStopCallback,
                              SensorConnectionCallback,
                              SensorDepthImageCallback,
                              SensorColorImageCallback,
                              SensorGestureCallback,
                              SensorFaceCallback,
                              SensorPerfCallback,
                              sensorTokenSource.Token
                              );
                     Debug.WriteLine("MainWindow.OnBrowserLoaded: Closing sensor");
                  }
                  catch (Exception ee)
                  {
                     Debug.WriteLine(ee);
                  }
               }, sensorTokenSource.Token);
            }
         });
      }

      private void SensorPerfCallback(double fps)
      {
         if (sensorGridVisible && !sensorStop)
            Dispatcher.Invoke(() =>
            {
               if (sensorTokenSource.Token.IsCancellationRequested) return;
               performanceTextBlock.Text = "FPS: " + fps.ToString(".##");
            });
      }

      private void SensorFaceCallback(PXCMFaceAnalysis faceAnalysis, PXCMFaceAnalysis.Detection.Data[] faceDetectionData)
      {
         try
         {
            if (faceDetectionData != null && faceDetectionData.Length != 0 && sensorGridVisible && !sensorStop)
            {
               Dispatcher.Invoke(() =>
               {
                  if (sensorTokenSource.Token.IsCancellationRequested)
                     return;

                  StringBuilder builder = new StringBuilder();
                  builder.Append(faceDetectionData.Length);
                  builder.Append(" face(s) found: ");
                  builder.Append(" => ");

                  int facesFound = 0;
                  foreach (var item in faceDetectionData)
                  {
                     builder.Append(item.confidence);
                     builder.Append("% ");

                     if (item.confidence > 90)
                        ++facesFound;
                  }

                  if (luaOnPCXFaces != null)
                     luaOnPCXFaces.Call(facesFound);

                  if (facesFound > 0) ResumeActivities();
                  if (sensorTokenSource.Token.IsCancellationRequested) return;

                  faceTextBlock.Text = builder.ToString();
               });
            }
            else
            {
               Dispatcher.Invoke(() =>
               {
                  faceTextBlock.Text = "0 face(s) found";
               });
            }
         }
         catch (Exception e)
         {
            Debug.WriteLine(e);
         }
      }

      private double RangeTransform(double x, double a, double b, double c, double d)
      {
         return c + (d - c) * ((x - a) / (b - a));
         //return c * (1 - (x - a) / (b - a)) + d * ((x - a) / (b - a));
      }

      private void SensorGestureCallback(PXCMGesture gesture, PXCMGesture.Gesture[] gestures, PXCMGesture.GeoNode[][] hands)
      {
         if (gestures != null && !sensorStop)
         {
            uint confidence = 0;
            var gestureLabel = PXCMGesture.Gesture.Label.LABEL_ANY;

            if (gestures[0].label > 0)
            {
               confidence = gestures[0].confidence;
               gestureLabel = gestures[0].label;
            }
            else if (gestures[1].label > 0)
            {
               confidence = gestures[1].confidence;
               gestureLabel = gestures[1].label;
            }

            //bool handInputEnabled = trackingGesturesTimer != null
            //    ? trackingGesturesTimer.Elapsed.TotalSeconds > 1.5
            //    : true;
            //if (handInputEnabled)
            //    trackingGesturesTimer = null;

            if (gestureLabel != PXCMGesture.Gesture.Label.LABEL_ANY &&
                gestureLabel != PXCMGesture.Gesture.Label.LABEL_POSE_BIG5 &&
                confidence > 80 &&
                !trackingHand)
            {
               ResumeActivities();

               //trackingGesturesTimer = null;
               //trackingGesturesTimer = Stopwatch.StartNew();

               //Debug.WriteLine("Gesture: Nav/Hand/Pose: " + gestureLabel + " " + confidence + "%");
               //switch (gestureLabel)
               //{
               //   case PXCMGesture.Gesture.Label.LABEL_NAV_SWIPE_DOWN: Browser.SendKeyEvent(0x0100, 0x28, 0); break;
               //   case PXCMGesture.Gesture.Label.LABEL_NAV_SWIPE_LEFT: Browser.SendKeyEvent(0x0100, 0x25, 0); break;
               //   case PXCMGesture.Gesture.Label.LABEL_NAV_SWIPE_RIGHT: Browser.SendKeyEvent(0x0100, 0x27, 0); break;
               //   case PXCMGesture.Gesture.Label.LABEL_NAV_SWIPE_UP: Browser.SendKeyEvent(0x0100, 0x26, 0); break;
               //   case PXCMGesture.Gesture.Label.LABEL_HAND_CIRCLE: Browser.SendKeyEvent(0x0100, 0x43, 0); break;
               //   case PXCMGesture.Gesture.Label.LABEL_HAND_WAVE: Browser.SendKeyEvent(0x0100, 0x57, 0); break;
               //   case PXCMGesture.Gesture.Label.LABEL_POSE_PEACE:
               //      Browser.SendKeyEvent(0x0100, 0x56, 0);
               //      Browser.ExecuteScriptAsync("(function(){window.alert('peaceee maaaaan!!!')})()");
               //      break;
               //   case PXCMGesture.Gesture.Label.LABEL_POSE_THUMB_DOWN: Browser.SendKeyEvent(0x0100, 0x44, 0); break;
               //   case PXCMGesture.Gesture.Label.LABEL_POSE_THUMB_UP: Browser.SendKeyEvent(0x0100, 0x4c, 0); break;
               //      //case PXCMGesture.Gesture.Label.LABEL_POSE_BIG5: Browser.SendKeyEvent(0x0100, 0x50, 0); break;
               //}

               if (luaOnPCXGesture != null)
                  luaOnPCXGesture.Call(gestureLabel.ToString());
            }
            else
            {
               bool handPositionFound = false;
               PXCMGesture.GeoNode handNode = new PXCMGesture.GeoNode();

               if ((hands[0][0].body == PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_LEFT ||
                   hands[0][0].body == PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_RIGHT) &&
                   hands[0][0].confidence == 100)
               {
                  handNode = hands[0][0];
                  handPositionFound = true;
               }
               else if ((hands[1][0].body == PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_LEFT ||
                   hands[1][0].body == PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_RIGHT) &&
                   hands[1][0].confidence == 100)
               {
                  handNode = hands[1][0];
                  handPositionFound = true;
               }

               if (handPositionFound)
               {
                  double canvasWidth = rootCanvas.ActualWidth;
                  double canvasHeight = rootCanvas.ActualHeight;
                  double imageX = 320.0 - handNode.positionImage.x; imageX /= 320.0;
                  double imageY = handNode.positionImage.y; imageY /= 240.0;
                  imageX = RangeTransform(imageX, 0.25, 0.75, 0, 1);
                  imageY = RangeTransform(imageY, 0.25, 0.75, 0, 1);

                  double canvasX = imageX * canvasWidth;
                  double canvasY = imageY * canvasHeight;

                  AddHandPosition(canvasX, canvasY);
                  var handPosition = AvgHandPosition();

                  bool handIsClosed = handNode.opennessState != PXCMGesture.GeoNode.Openness.LABEL_OPEN; // || 

                  Dispatcher.Invoke(() =>
                  {
                     handEllipse.StrokeThickness = 5.0;
                     handEllipse.Fill = Brushes.Transparent;
                     handEllipse.Stroke = handIsClosed ? Brushes.Red : Brushes.Blue;
                     handEllipse.Width = handEllipse.Height = handIsClosed ? 50 : 50;

                     Canvas.SetLeft(handEllipse, handPosition.x - handEllipse.Width * 0.5);
                     Canvas.SetTop(handEllipse, handPosition.y - handEllipse.Height * 0.5);

                     CefEventFlags f = CefEventFlags.LeftMouseButton;

                     if (trackingHand)
                     {
                        if (handPosition.CalculateDistanceTo(starDragPosition) > 25)
                        {
                           Debug.WriteLine("SensorGestureCallback: hand move: " + (int)handPosition.x + " " + (int)handPosition.y);

                           //Browser.ManagedCefBrowserAdapter.OnMouseButton((int)handPosition.x, (int)handPosition.y, (int)MouseButton.Left, false, 1, CefEventFlags.LeftMouseButton);
                           Browser.ManagedCefBrowserAdapter.OnMouseMove((int)handPosition.x, (int)handPosition.y, false, CefEventFlags.LeftMouseButton);

                           //ClickOnPointTool.ClickOnPoint(windowHandle, new Point((int)handPosition.x, (int)handPosition.y), 0x1);
                           //SendMessage(WindowHandle, WM_MOUSEMOVE, (int)handPosition.x, (int)handPosition.y);
                           //TouchMouseUpdate(WindowHandle, (int)handPosition.x, (int)handPosition.y);
                        }
                        else
                           Browser.ManagedCefBrowserAdapter.OnMouseMove((int)handPosition.x, (int)handPosition.y, false, CefEventFlags.None);
                     }

                     if (handIsClosed && !trackingHand) // && handInputEnabled)
                     {
                        Debug.WriteLine("SensorGestureCallback: hand pressed: " + (int)handPosition.x + " " + (int)handPosition.y);

                        Browser.ManagedCefBrowserAdapter.SendFocusEvent(true);
                        Browser.ManagedCefBrowserAdapter.OnMouseButton((int)handPosition.x, (int)handPosition.y, (int)MouseButton.Left, true, 1, CefEventFlags.LeftMouseButton);
                        Browser.ManagedCefBrowserAdapter.OnMouseButton((int)handPosition.x, (int)handPosition.y, (int)MouseButton.Left, false, 1, CefEventFlags.LeftMouseButton);

                        //ClickOnPointTool.ClickOnPoint(windowHandle, new Point((int)handPosition.x, (int)handPosition.y), 0x2);
                        //SendMessage(WindowHandle, WM_MOUSEDOWN, (int)handPosition.x, (int)handPosition.y);
                        //TouchMouseDown(WindowHandle, (int)handPosition.x, (int)handPosition.y);

                        starDragPosition = handPosition;
                        trackingHand = true;
                     }
                     else if (!handIsClosed && trackingHand)
                     {
                        Debug.WriteLine("SensorGestureCallback: hand released: " + (int)handPosition.x + " " + (int)handPosition.y);

                        //ClickOnPointTool.ClickOnPoint(windowHandle, new Point((int)handPosition.x, (int)handPosition.y), 0x4);
                        //TouchMouseUp(WindowHandle, (int)handPosition.x, (int)handPosition.y);

                        Browser.ManagedCefBrowserAdapter.OnMouseButton((int)handPosition.x, (int)handPosition.y, (int)MouseButton.Left, false, 1, CefEventFlags.LeftMouseButton);
                        Browser.ManagedCefBrowserAdapter.OnMouseButton((int)handPosition.x, (int)handPosition.y, (int)MouseButton.Left, true, 1, CefEventFlags.LeftMouseButton);
                        Browser.ManagedCefBrowserAdapter.SendFocusEvent(false);

                        //SendMessage(WindowHandle, WM_MOUSEUP, (int)handPosition.x, (int)handPosition.y);

                        trackingHand = false;
                     }
                  });

                  prevHandPosition = handPosition;
               }
            }
         }
      }

      private BitmapImage ConvertToBitmapImage(System.Drawing.Bitmap bitmap)
      {
         using (MemoryStream memory = new MemoryStream())
         {
            bitmap.Save(memory, ImageFormat.Bmp);
            //bitmap.Save(memory, ImageFormat.Png);
            memory.Position = 0;
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = memory;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            return bitmapImage;
         }
      }

      private void SensorColorImageCallback(PXCMImage image)
      {
         //return;
         if (sensorGridVisible && !sensorStop && !sensorTokenSource.Token.IsCancellationRequested && images[0] == null)
         {
            try
            {
               PXCMImage.ImageData data;
               if (image.AcquireAccess(PXCMImage.Access.ACCESS_READ, PXCMImage.ColorFormat.COLOR_FORMAT_RGB32, out data) >= pxcmStatus.PXCM_STATUS_NO_ERROR)
               {
                  if (sensorStop) return;
                  images[0] = ConvertToBitmapImage(data.ToBitmap(image.info.width, image.info.height));
                  image.ReleaseAccess(ref data);
                  images[0].Freeze();

                  if (sensorStop) return;
                  Dispatcher.Invoke(() =>
                  {
                     if (!sensorTokenSource.Token.IsCancellationRequested && !sensorStop)
                     {
                        colorImage.Source = images[0];
                        images[0] = null;
                     }
                  }, DispatcherPriority.ContextIdle);
               }
            }
            catch (Exception e)
            {
               Debug.WriteLine(e);
            }
         }
      }

      private void SensorDepthImageCallback(PXCMImage image)
      {
         //return;
         if (sensorGridVisible && !sensorStop && !sensorTokenSource.Token.IsCancellationRequested && images[1] == null)
         {
            try
            {
               PXCMImage.ImageData data;
               if (image.AcquireAccess(PXCMImage.Access.ACCESS_READ, PXCMImage.ColorFormat.COLOR_FORMAT_RGB32, out data) >= pxcmStatus.PXCM_STATUS_NO_ERROR)
               {
                  if (sensorStop) return;
                  images[1] = ConvertToBitmapImage(data.ToBitmap(image.info.width, image.info.height));
                  image.ReleaseAccess(ref data);
                  images[1].Freeze();

                  if (sensorStop) return;
                  Dispatcher.Invoke(() =>
                  {
                     if (!sensorTokenSource.Token.IsCancellationRequested && !sensorStop)
                     {
                        depthImage.Source = images[1];
                        images[1] = null;
                     }
                  }, DispatcherPriority.ContextIdle);
               }
            }
            catch (Exception e)
            {
               Debug.WriteLine(e);
            }
         }
      }

      private void SensorConnectionCallback(bool obj)
      {
         Debug.WriteLine("SensorConnectionCallback: Connected " + obj);
      }

      private bool SensorStopCallback()
      {
         return sensorStop;
      }
   }
}
