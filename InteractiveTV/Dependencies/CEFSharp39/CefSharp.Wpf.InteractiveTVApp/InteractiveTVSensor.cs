﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CefSharp.Wpf.InteractiveTVApp
{
    public class InteractiveTVSensor
    {
        public InteractiveTVSensor()
        {
        }

        public void SimplePipeline(
            Func<bool> stopCallback,
            Action<bool> connectionCallback,
            Action<PXCMImage> depthCallback,
            Action<PXCMImage> colorCallback,
            Action<PXCMGesture, PXCMGesture.Gesture[], PXCMGesture.GeoNode[][]> gestureCallback,
            Action<PXCMFaceAnalysis, PXCMFaceAnalysis.Detection.Data[]> faceCallback,
            Action<double> perfCallback,
            CancellationToken token
            )
        {
            bool sts = true;

            var pp = new UtilMPipeline(App.PXCMSessionInstance.Value);
            pp.EnableGesture();
            pp.EnableFaceLocation();

            var stopwatch = new Stopwatch();

            var nodes = new PXCMGesture.GeoNode[2][] { new PXCMGesture.GeoNode[11], new PXCMGesture.GeoNode[11] };
            var gestures = new PXCMGesture.Gesture[2];
            var faceDetections = new List<PXCMFaceAnalysis.Detection.Data>();

            Debug.WriteLine("Init Started");
            if (pp.Init())
            {
                Debug.WriteLine("Streaming");
                connectionCallback(true);

                stopwatch.Start();
                while (!stopCallback())
                {
                    if (pp.AcquireFrame(true) && (token == null ? true : !token.IsCancellationRequested))
                    {
                        if (depthCallback != null)
                        {
                            PXCMImage depthImage = pp.QueryImage(PXCMImage.ImageType.IMAGE_TYPE_DEPTH);
                            depthCallback(depthImage);
                        }

                        if (colorCallback != null)
                        {
                            PXCMImage colorImage = pp.QueryImage(PXCMImage.ImageType.IMAGE_TYPE_COLOR);
                            colorCallback(colorImage);
                        }

                        if (gestureCallback != null)
                        {
                            var gesture = pp.QueryGesture();
                            gesture.QueryGestureData(0, PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_PRIMARY, 0, out gestures[0]);
                            gesture.QueryGestureData(0, PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_SECONDARY, 0, out gestures[1]);
                            gesture.QueryNodeData(0, PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_PRIMARY, nodes[0]);
                            gesture.QueryNodeData(0, PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_SECONDARY, nodes[1]);
                            gesture.QueryNodeData(0, PXCMGesture.GeoNode.Label.LABEL_BODY_ELBOW_PRIMARY, out nodes[0][nodes.Length - 1]);
                            gesture.QueryNodeData(0, PXCMGesture.GeoNode.Label.LABEL_BODY_ELBOW_SECONDARY, out nodes[1][nodes.Length - 1]);
                            gestureCallback(gesture, gestures, nodes);
                        }

                        if (faceCallback != null)
                        {
                            var face = pp.QueryFace();
                            if (face != null)
                            {
                                for (uint i = 0; ; i++)
                                {
                                    int fid; ulong ts;
                                    if (face.QueryFace(i, out fid, out ts) < pxcmStatus.PXCM_STATUS_NO_ERROR)
                                        break;

                                    /* Retrieve face location data */
                                    PXCMFaceAnalysis.Detection.Data ddata;
                                    PXCMFaceAnalysis.Detection faceDetection = face.DynamicCast<PXCMFaceAnalysis.Detection>(PXCMFaceAnalysis.Detection.CUID);
                                    if (faceDetection.QueryData(fid, out ddata) >= pxcmStatus.PXCM_STATUS_NO_ERROR)
                                        faceDetections.Add(ddata);
                                }
                                faceCallback(face, faceDetections.ToArray());
                            }
                            else
                            {
                                faceCallback(null, null);
                            }

                            faceDetections.Clear();
                        }

                        pp.ReleaseFrame();
                        perfCallback(1.0 / stopwatch.Elapsed.TotalSeconds);
                        stopwatch.Restart();
                    }
                    else
                    {
                        connectionCallback(false);
                        break;
                    }
                }
            }
            else
            {
                Debug.WriteLine("Init Failed");
                sts = false;
            }

            pp.Close();
            pp.Dispose();

            if (sts) 
                Debug.WriteLine("Stopped");
        }
    }
}
