﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;

namespace Labwork02.Server
{
    using IndividualTask;
    using System.Collections.Concurrent;

    class Log
    {
        private static readonly object syncObject = new object();
        public static void WriteLine(string format, params object[] args)
        {
            lock (syncObject)
            {
                Debug.WriteLine(string.Format(format, args));
            }
        }
        public static void Write(string format, params object[] args)
        {
            lock (syncObject)
            {
                Debug.Write(string.Format(format, args));
            }
        }
    }

    class Program
    {
        private const string CastClientCommandPrefix = "-cast:";
        private const string GetIndexClientCommand = "-get-index";
        private const string GetNameClientCommand = "-get-name";
        private const string GetTimeClientCommand = "-get-time";
        private const string GoodbyeClientCommand = "-bye";
        private const string ReportExecStatsClientCommand = "-res";
        private const string ReportStatusClientCommand = "-stats";
        private const string ReportHashClientCommand = "-hash";
        private const string TaskClientCommandPrefix = "-task:";

        private List<Socket> clientSockets = new List<Socket>();
        private byte[] clientMessageBuffer = new byte[1024];


        private Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        private ConcurrentDictionary<Socket, string> clientNameBySocket = new ConcurrentDictionary<Socket, string>();
        private ConcurrentDictionary<Socket, string> clientMessagesBySocket = new ConcurrentDictionary<Socket, string>();

        Processor taskProcessor = new Processor(2);
        ProcessorTaskCache taskCache = new ProcessorTaskCache();

        private void SetupServerSocket() 
        {
            Console.WriteLine("Setting up the server...");

            serverSocket.Bind(new IPEndPoint(IPAddress.Any, 100));
            serverSocket.Listen(255); // pending connections 
            serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null); // null for object state
        }

        private void AcceptCallback(IAsyncResult acceptResult)
        {
            Console.WriteLine("Client connected");

            var newClientSocket = serverSocket.EndAccept(acceptResult);
            clientSockets.Add(newClientSocket);
            newClientSocket.BeginReceive(clientMessageBuffer, 0, clientMessageBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), newClientSocket);
            serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null); // null for object state

        }

        private async void ReceiveCallback(IAsyncResult receiveResult)
        {
            var clientSocket = (Socket)receiveResult.AsyncState;
            if (clientSocket == null) return;

            try
            {
                int receivedDataSize = clientSocket.EndReceive(receiveResult);
                byte[] receivedData = new byte[receivedDataSize];

                Array.Copy(clientMessageBuffer, receivedData, receivedData.Length);
                string text = Encoding.ASCII.GetString(receivedData);

                //Console.WriteLine("Text received: " + text);

                string response = string.Empty;

                try
                {
                    if (text.ToLower() == GetIndexClientCommand)
                    {
                        response = clientSockets.IndexOf(clientSocket).ToString();
                    }
                    else if (text.ToLower() == GetNameClientCommand)
                    {
                        if (clientNameBySocket.ContainsKey(clientSocket))
                            response = clientNameBySocket[clientSocket];
                        else
                        {
                            response = ClientNameProvider.Instance.NextName;
                            while (!clientNameBySocket.TryAdd(clientSocket, (string)response.Clone()))
                                response = ClientNameProvider.Instance.NextName;
                        }
                    }
                    else if (text.ToLower() == GetTimeClientCommand)
                    {
                        response = DateTime.Now.ToLongTimeString();
                    }
                    else if (text.ToLower() == GoodbyeClientCommand)
                    {
                        try
                        {
                            clientSocket.BeginDisconnect(false, new AsyncCallback(DisconnectCallback), clientSocket);
                            clientSocket = null;
                        }
                        catch (Exception)
                        {
                            HandleClientSocketError(clientSocket);
                        }
                    }
                    else if (text.ToLower() == ReportExecStatsClientCommand)
                    {
                        taskProcessor.ReportExecutionStatistics(out response);
                    }
                    else if (text.ToLower() == ReportStatusClientCommand)
                    {
                        taskProcessor.ReportStatus(out response);
                    }
                    else if (text.ToLower() == ReportHashClientCommand)
                    {
                        taskCache.ReportCachingStatistics(out response);
                    }
                    else if (text.StartsWith(TaskClientCommandPrefix))
                    {
                        float taskRuntime = float.Parse(text.Substring(TaskClientCommandPrefix.Length));
                        var taskCached = taskCache.PullTask(taskRuntime);
                        await taskProcessor.PushTaskAsync(taskCached);
                        response = "-pushed:" + taskProcessor.PushableQueue.Index;

                    }
                    else if (text.StartsWith(CastClientCommandPrefix))
                    {
                        var castParams = text.Substring(CastClientCommandPrefix.Length);
                        var castClients = castParams.Substring(0, castParams.IndexOf(':'));
                        var castClientNames = castClients.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        if (castClientNames != null && castClientNames.Length > 0)
                        {
                            var sender = clientNameBySocket[clientSocket];
                            var castMessage = castParams.Substring(castClients.Length + 1);
                            foreach (var clientName in castClientNames)
                            {
                                var addressed = clientNameBySocket.Single(x => x.Value == clientName).Key;
                                if (addressed != null && !clientMessagesBySocket.ContainsKey(addressed))
                                    clientMessagesBySocket.TryAdd(addressed, sender + ":" + castMessage);
                            }
                        }

                        response = "-processed";
                    }
                    else
                    {
                        Console.WriteLine("Invalid request: " + text);
                        response = "Invalid request";
                    }
                }
                catch (Exception e)
                {
                    response = e.Message;
                }

                if (!string.IsNullOrEmpty(response))
                {
                    if (clientMessagesBySocket.ContainsKey(clientSocket))
                    {
                        string message;
                        clientMessagesBySocket.TryRemove(clientSocket, out message);
                        response += "|" + message;
                    }

                    byte[] sendData = Encoding.ASCII.GetBytes(response);
                    clientSocket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(SendCallback), clientSocket);
                }
            }
            catch (SocketException)
            {
                HandleClientSocketError(clientSocket);
            }
        }

        private void DisconnectCallback(IAsyncResult disconnectResult)
        {
            var clientSocket = (Socket)disconnectResult.AsyncState;

            try
            {
                var clientName = clientNameBySocket[clientSocket];
                Console.WriteLine(clientName + " has disconnected");

                clientSocket.EndDisconnect(disconnectResult);
                clientSockets.Remove(clientSocket);
                clientSocket.Dispose();

                foreach (var keyValuePair in clientNameBySocket)
                {
                    if (keyValuePair.Value != clientName)
                    {
                        if (clientMessagesBySocket.ContainsKey(keyValuePair.Key))
                        {
                            try
                            {
                                clientMessagesBySocket[keyValuePair.Key] += "|" + clientName + " died";
                            }
                            catch (Exception)
                            {
                                clientMessagesBySocket.TryAdd(keyValuePair.Key, "|" + clientName + " died");
                            }
                        }
                    }
                }


                clientSocket = null;
            }
            catch (Exception)
            {
                HandleClientSocketError(clientSocket);
            }
        }

        private void SendCallback(IAsyncResult sendResult)
        {
            var clientSocket = (Socket)sendResult.AsyncState;
            clientSocket.EndSend(sendResult);
            clientSocket.BeginReceive(clientMessageBuffer, 0, clientMessageBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), clientSocket);
        }

        private void HandleClientSocketError(Socket clientSocket)
        {
            try
            {
                var clientName = clientNameBySocket[clientSocket];
                Console.WriteLine(clientName + " has disconnected (forced)");

                foreach (var keyValuePair in clientNameBySocket)
                {
                    if (keyValuePair.Value != clientName)
                    {
                        if (clientMessagesBySocket.ContainsKey(keyValuePair.Key))
                        {
                            try
                            {
                                clientMessagesBySocket[keyValuePair.Key] += "|" + clientName + " died";
                            }
                            catch (Exception)
                            {
                                clientMessagesBySocket.TryAdd(keyValuePair.Key, "|" + clientName + " died");
                            }
                        }
                        else
                        {
                            clientMessagesBySocket.TryAdd(keyValuePair.Key, "|" + clientName + " died");
                        }
                    }
                }

                clientSocket.Disconnect(false);
                clientSocket.Dispose();
            }
            finally
            {
                clientSockets.Remove(clientSocket);
                clientSocket = null;
            }
        }

        private static string ExitMessage { get { return "-exit"; } }

        private static void PrintUsage()
        {
            Console.WriteLine(
                "TCP server application\n" +
                "It keeps adding new clients, receiving their messages, and handling them, while processing users input\nAvailable server commands:\n" + 
                "\t - enter '" + ExitMessage + "' to terminate app"
                );
        }

        static void Main(string[] args)
        {
            Console.Title = "VladSerhiienko, Labwork02: Server";

            Program program = new Program();
            program.SetupServerSocket();

            PrintUsage();
            string userInput = Console.ReadLine();
            while (userInput != ExitMessage)
                PrintUsage();
        }
    }
}
