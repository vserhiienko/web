﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labwork02.Server
{
    public class ClientNameProvider
    {
        public static ClientNameProvider Instance
        {
            get { return IndividualTask.LazySingletonFactory<ClientNameProvider>.Instance; }
        }

        private Random random = new Random();
        public List<string> Names { get; protected set; }

        public ClientNameProvider()
        {
            Names = new List<string>(File.ReadAllLines("ClientNames.txt"));
        }

        public string NextName
        {
            get
            {
                return Names[random.Next(Names.Count - 1)];
            }
        }

    }
}
