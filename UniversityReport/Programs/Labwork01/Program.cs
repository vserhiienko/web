﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Labwork01
{
    class Log
    {
        private static readonly object syncObject = new object();
        public static void WriteLine(string format, params object[] args)
        {
            lock (syncObject)
            {
                Debug.WriteLine(string.Format(format, args));
            }
        }
        public static void Write(string format, params object[] args)
        {
            lock (syncObject)
            {
                Debug.Write(string.Format(format, args));
            }
        }
    }


    class ProcessorTask : IDisposable
    {
        private static int nextIndex = 0;
        private static readonly object syncObject = new object();
        private static readonly Random runtimeGenerator = new Random();

        public int index;
        private float runtimeSeconds;

        public ProcessorTask()
        {
            lock (syncObject)
            {
                index = nextIndex++;
                runtimeSeconds = getRandomRuntime();
            }
        }

        public static float getRandomRuntime()
        {
            return (float)runtimeGenerator.NextDouble() * 0.2f;
        }

        public void Execute()
        {
            Thread.Sleep(TimeSpan.FromSeconds(runtimeSeconds));
        }

        public void Dispose()
        {
            Log.WriteLine("Disposing {0} task", index);
        }
    }

    class ProcessorTaskFactory
    {
        public static ProcessorTask New
        {
            get
            {
                Log.WriteLine("Generating new task...");
                return new ProcessorTask();
            }
        }
    }

    class ProcessorExecutionStatistics
    {
        public int tasksProcessedTotal;
        public int[] tasksProcessedOnQueues;

        public void ReportStatus(bool console = false)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("\n\t>> {0} was processed\n"
                , tasksProcessedTotal
                );

            int queueIndex = 0;
            foreach (var sz in tasksProcessedOnQueues)
            {
                float p = (float)sz / (float)tasksProcessedTotal * 100.0f;
                sb.AppendFormat("\t>> queue@{0} processed {1} tasks, that is {2}% from total\n"
                    , queueIndex
                    , sz
                    , p.ToString("0.00")
                    );
                queueIndex++;
            }

            string message = sb.ToString();
            sb = null;

            if (console) Console.WriteLine(message);
            else Log.WriteLine(message);
        }
    }

    class ProcessTaskQueue : ConcurrentQueue<ProcessorTask>
    {
        public int index;
        public ProcessTaskQueue(int index) : base()
        {
            this.index = index;
        }
    }

    class Processor
    {
        private ProcessTaskQueue[] queues;

        public ProcessTaskQueue PushableQueue
        {
            get
            {
                return queues.Aggregate((q, x) => ((q == null || x.Count < q.Count) ? x : q));
            }
        }
        public ProcessTaskQueue ProcessableQueue
        {
            get
            {
                return queues.Aggregate((q, x) => ((q == null || x.Count >= q.Count) ? x : q));
            }
        }

        public Processor(int queueCount)
        {
            queues = new ProcessTaskQueue[queueCount];
            foreach (var i in Enumerable.Range(0, queueCount))
                queues[i] = new ProcessTaskQueue(i);
        }

        public void PushTask(ProcessorTask task)
        {
            if (task == null) return;
            var q = PushableQueue;
            Log.WriteLine("Pushing {0} task to {1} queue", task.index, q.index);
            PushableQueue.Enqueue(task);
        }

        public void ReportStatus()
        {
            var sb = new StringBuilder();
            foreach (var q in queues)
                sb.AppendFormat("\t>> queue{0} has {1} pending tasks\n", q.index, q.Count);
            Console.WriteLine(sb.ToString());
            sb = null;
        }


        private static bool executeSync = true;
        private bool printStatisticsReport = false;

        public void RequestStatisticsReport()
        {
            printStatisticsReport = true;
        }

        public async Task<ProcessorExecutionStatistics> Launch(CancellationToken cancellationToken)
        {
            return await Task<ProcessorExecutionStatistics>.Run(() =>
                {
                    var stats = new ProcessorExecutionStatistics();
                    stats.tasksProcessedOnQueues = new int[queues.Length];
                    stats.tasksProcessedTotal = 0;

                    var q = ProcessableQueue;
                    while (!q.IsEmpty && !cancellationToken.IsCancellationRequested)
                    {
                        if (executeSync)
                        {
                            ProcessorTask task;
                            if (q.TryDequeue(out task) && task != null)
                            {
                                Log.WriteLine("Executing {0} task from {1} queue", task.index, q.index);

                                task.Execute();
                                task.Dispose();
                                task = null;

                                Interlocked.Increment(ref stats.tasksProcessedOnQueues[q.index]);
                                Interlocked.Increment(ref stats.tasksProcessedTotal);
                            }
                        }
                        else
                        {
                            Task.Run(() =>
                            {
                                ProcessorTask task;
                                if (q.TryDequeue(out task) && task != null)
                                {
                                    task.Execute();
                                    task.Dispose();
                                    task = null;

                                    Interlocked.Increment(ref stats.tasksProcessedOnQueues[q.index]);
                                    Interlocked.Increment(ref stats.tasksProcessedTotal);
                                }
                            });
                        }

                        if (printStatisticsReport)
                        {
                            printStatisticsReport = false;
                            stats.ReportStatus(true);
                        }

                        q = ProcessableQueue;
                    }

                    return stats;
                });
        }

    }


    class Application
    {
        const int VK_RETURN = 0x0D;
        const int WM_KEYDOWN = 0x100;
        [DllImport("User32.Dll", EntryPoint = "PostMessageA")]
        private static extern bool PostMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

        private static void PrintUsage()
        {
            Console.WriteLine("\nPress: \t'P' - processor queue statuses,\n\t'S' - execution statistics,\n\t'E' - cancel processor execution");
        }


        static void Main(string[] args)
        {
            const int intialPayloadSize = 100;

            // generate initial payload
            var processor = new Processor(2);
            foreach (var i in Enumerable.Range(0, intialPayloadSize))
                processor.PushTask(ProcessorTaskFactory.New);

            var timer = Stopwatch.StartNew();
            var cancellationTokenSrc = new CancellationTokenSource();

            var processorExecutionTask = Task.Run(() =>
                {
                    var task = processor.Launch(cancellationTokenSrc.Token);
                    var tasks = new Task[] { task };

                    try
                    {
                        Task.WaitAll(tasks, cancellationTokenSrc.Token);
                    }
                    catch (OperationCanceledException)
                    {
                        Log.WriteLine("Processor execution was cancelled");
                        Console.WriteLine("Processor execution was cancelled");
                    }
                    catch (Exception e)
                    {
                    // handle generic exeptions
                    Log.WriteLine(e.Message);
                    }
                    finally
                    {
                        cancellationTokenSrc.Dispose();
                        cancellationTokenSrc = null;

                        task.Result.ReportStatus(true);
                        task.Result.ReportStatus(false);
                    }
                }, cancellationTokenSrc.Token);

            var additionalPayloadTask = Task.Run(() =>
                {
                    const int runtimeSeconds = 120;
                    while (timer.Elapsed.Seconds < runtimeSeconds)
                    {
                        if (cancellationTokenSrc != null && !cancellationTokenSrc.Token.IsCancellationRequested)
                        {
                            Thread.Sleep(TimeSpan.FromSeconds(ProcessorTask.getRandomRuntime()));
                            processor.PushTask(ProcessorTaskFactory.New);
                        }
                        else
                        {
                            Log.WriteLine("Payload generation was cancelled, breaking the thread...");
                            break;
                        }
                    }
                }, cancellationTokenSrc.Token);

            ThreadPool.QueueUserWorkItem((o) =>
            {
                Task.WaitAll(processorExecutionTask);
                if (cancellationTokenSrc != null)
                {
                    Log.WriteLine("Cancelling console");
                    PostMessage(Process.GetCurrentProcess().MainWindowHandle, WM_KEYDOWN, VK_RETURN, 0);
                }
            });

            PrintUsage();

            bool cancellationRequested = false;
            Console.Title = "VladSerhiienko, Labwork01";
            while (!cancellationRequested)
            {
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.P: processor.ReportStatus(); break;
                    case ConsoleKey.S: processor.RequestStatisticsReport(); break;
                    case ConsoleKey.E: cancellationRequested = true; break;
                    default: PrintUsage(); break;
                }
            }

            if (cancellationTokenSrc != null)
                cancellationTokenSrc.Cancel();

            Console.WriteLine("\nRuntime = {0:#.##} s", timer.Elapsed.TotalSeconds);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();

        }
    }
}
