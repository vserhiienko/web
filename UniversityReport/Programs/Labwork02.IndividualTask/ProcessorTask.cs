﻿using System;
using System.Threading;
using System.Web.Script.Serialization;

namespace Labwork02.IndividualTask
{
    public class ProcessorTask 
    {
        private static int nextIndex = 0;
        private static readonly object syncObject = new object();
        private static readonly Random runtimeGenerator = new Random();

        public int Index { get; set; }
        public float RuntimeSeconds { get; set; }

        public ProcessorTask()
        {
            lock (syncObject)
            {
                Index = nextIndex++;
                RuntimeSeconds = RandomRuntimeSeconds;
            }
        }

        public static float RandomRuntimeSeconds
        {
            get { return (float)runtimeGenerator.NextDouble() * 0.2f; }
        }

        public void Execute()
        {
            Thread.Sleep(TimeSpan.FromSeconds(RuntimeSeconds));
        }

    }
}
