﻿using System.Web.Script.Serialization;

namespace Labwork02.IndividualTask
{
    public class ProcessorTaskJsonSerializer
    {
        static JavaScriptSerializer jsonSerializer;

        static ProcessorTaskJsonSerializer()
        {
            jsonSerializer = new JavaScriptSerializer();
        }

        public string Serialize(ProcessorTask task)
        {
            return jsonSerializer.Serialize(task);
        }
    }
}
