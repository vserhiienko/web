﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Labwork02.IndividualTask
{
    public class Processor : ProcessorExecutionStatistics
    {
        private bool executing = false;
        private bool printStatisticsReport = false;
        private ProcessorTaskQueue[] queues;
        CancellationTokenSource cancellationTokenSource;

        public ProcessorTaskQueue PushableQueue
        {
            get
            {
                Debug.Assert(queues != null && queues.Length > 0);
                return queues.Aggregate((q, x) => ((q == null || x.Count < q.Count) ? x : q));
            }
        }
        public ProcessorTaskQueue ProcessableQueue
        {
            get
            {
                Debug.Assert(queues != null && queues.Length > 0);
                return queues.Aggregate((q, x) => ((q == null || x.Count >= q.Count) ? x : q));
            }
        }

        public Processor(int queueCount) 
            : base(queueCount)
        {
            cancellationTokenSource = new CancellationTokenSource();
            queues = new ProcessorTaskQueue[queueCount];
            foreach (var i in Enumerable.Range(0, queueCount))
                queues[i] = new ProcessorTaskQueue(i);
        }

        public async Task<ProcessorExecutionStatistics> PushTaskAsync(ProcessorTask task)
        {
            if (task == null) return null;

            PushableQueue.Enqueue(task);
            var statsAsync = Launch(cancellationTokenSource.Token);
            return statsAsync != null ? await statsAsync : null;
        }

        public void ReportStatus()
        {
            var sb = new StringBuilder();
            foreach (var q in queues)
                sb.AppendFormat("\t>> queue{0} has {1} pending tasks\n", q.Index, q.Count);
            Console.WriteLine(sb.ToString());
            sb = null;
        }

        public void ReportStatus(out string status)
        {
            var sb = new StringBuilder();
            foreach (var q in queues)
                sb.AppendFormat("queue{0}-{1},", q.Index, q.Count);
            status = sb.ToString();
            sb = null;
        }

        public void ReportStatistics(out string status)
        {
            ReportExecutionStatistics(out status);
        }

        public void RequestStatisticsReport()
        {
            printStatisticsReport = true;
        }

        public async Task<ProcessorExecutionStatistics> Launch(CancellationToken cancellationToken)
        {
            if (executing) return null;

            return await Task.Run(() =>
            {
                if (executing)
                    return this;
                else
                {
                    executing = true;

                    try
                    {
                        var q = ProcessableQueue;
                        while (q != null && !q.IsEmpty && !cancellationToken.IsCancellationRequested)
                        {
                            ProcessorTask task;
                            if (q.TryDequeue(out task) && task != null)
                            {
                                Log.WriteLine("Executing {0} task from {1} queue", task.Index, q.Index);

                                task.Execute();
                                task = null;

                                Interlocked.Increment(ref tasksProcessedOnQueues[q.Index]);
                                Interlocked.Increment(ref tasksProcessedTotal);
                            }

                            if (printStatisticsReport)
                            {
                                printStatisticsReport = false;
                                ReportExecutionStatistics(true);
                            }

                            q = ProcessableQueue;
                        }
                    }
                    finally
                    {
                        executing = false;
                    }
                }

                return this;
            });
        }

    }
}
