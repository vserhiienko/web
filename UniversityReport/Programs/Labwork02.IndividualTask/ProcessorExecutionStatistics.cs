﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labwork02.IndividualTask
{
    public class ProcessorExecutionStatistics
    {
        protected int tasksProcessedTotal;
        protected int[] tasksProcessedOnQueues;

        public ProcessorExecutionStatistics(int queueCount)
        {
            tasksProcessedOnQueues = new int[queueCount];
        }

        public void ReportExecutionStatistics(bool console = false)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("\n\t>> {0} was processed\n"
                , tasksProcessedTotal
                );

            int queueIndex = 0;
            foreach (var sz in tasksProcessedOnQueues)
            {
                float p = (float)sz / (float)tasksProcessedTotal * 100.0f;
                sb.AppendFormat("\t>> queue@{0} processed {1} tasks, that is {2}% from total\n"
                    , queueIndex
                    , sz
                    , p.ToString("0.00")
                    );
                queueIndex++;
            }

            string message = sb.ToString();
            sb = null;

            if (console) Console.WriteLine(message);
            else Log.WriteLine(message);
        }

        public void ReportExecutionStatistics(out string stats)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("S-{0}:"
                , tasksProcessedTotal
                );

            int queueIndex = 0;
            foreach (var sz in tasksProcessedOnQueues)
            {
                float p = (float)sz / (float)tasksProcessedTotal * 100.0f;
                sb.AppendFormat("queue@{0}-{1}-{2}%,"
                    , queueIndex
                    , sz
                    , p.ToString("0.00")
                    );
                queueIndex++;
            }

            stats = sb.ToString();
            sb = null;
        }
    }
}
