﻿namespace Labwork02.IndividualTask
{
    using System.Collections.Concurrent;
    public class ProcessorTaskQueue : ConcurrentQueue<ProcessorTask>
    {
        public int Index { get; set; }
        public ProcessorTaskQueue(int index) : base()
        {
            Index = index;
        }
    }
}
