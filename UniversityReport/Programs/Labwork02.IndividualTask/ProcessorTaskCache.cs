﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Labwork02.IndividualTask
{
    public class ProcessorTaskCache
    {
        static int DefaultHashingAccuracy = 10000;

        protected int matchCount;               // when hash matches existing tasks
        protected int mismatchCount;            // when mismatch occures
        protected int racingCollisionCount;     // when mismatch occures on other threads
        protected int totalAttemptCount;        // total count of pull requests
        private readonly ConcurrentDictionary<int, ProcessorTask> taskCache;

        public int HashingAccuracy { get; set; }

        public ProcessorTaskCache()
        {
            HashingAccuracy = DefaultHashingAccuracy;
            taskCache = new ConcurrentDictionary<int, ProcessorTask>();
        }

        public ProcessorTask PullTask(float desiredRuntimeSeconds)
        {
            int hash = (int)(desiredRuntimeSeconds * HashingAccuracy);
            Interlocked.Increment(ref totalAttemptCount);

            if (taskCache.ContainsKey(hash))
            {
                Interlocked.Increment(ref matchCount);
                return taskCache[hash];
            }

            Interlocked.Increment(ref mismatchCount);
            var task = SimpleProcessorTaskFactory.Instance.NewTask;
            task.RuntimeSeconds = desiredRuntimeSeconds;
            if (!taskCache.TryAdd(hash, task))
            {
                Interlocked.Increment(ref racingCollisionCount);
                return taskCache[hash];
            }
            else
                return task;
        }

        public void ReportCachingStatistics()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("\t>> caching: total {0} => matches {1}, mismatches {2} (racing {3})"
                , totalAttemptCount
                , matchCount
                , mismatchCount
                , racingCollisionCount
                );
            Console.WriteLine(sb.ToString());
            sb = null;
        }

        public void ReportCachingStatistics(out string stats)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("caching: total {0} => matches {1}, mismatches {2} (racing {3})"
                , totalAttemptCount
                , matchCount
                , mismatchCount
                , racingCollisionCount
                );
            stats = sb.ToString();
            sb = null;
        }
    }
}
