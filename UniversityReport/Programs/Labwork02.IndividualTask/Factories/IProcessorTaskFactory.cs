﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labwork02.IndividualTask
{
    public interface IProcessorTaskFactory
    {
        ProcessorTask NewTask { get; }
    }
}
