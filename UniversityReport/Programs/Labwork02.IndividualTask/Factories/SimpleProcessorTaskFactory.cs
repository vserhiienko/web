﻿namespace Labwork02.IndividualTask
{
    public class SimpleProcessorTaskFactory : IProcessorTaskFactory
    {
        public static IProcessorTaskFactory Instance
        {
            get { return LazySingletonFactory<SimpleProcessorTaskFactory>.Instance; }
        }

        public ProcessorTask NewTask
        {
            get
            {
                Log.WriteLine("Generating new task...");
                return new ProcessorTask();
            }
        }
    }
}
