﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labwork02.IndividualTask
{
    public class ProcessorLongTaskFactory : IProcessorTaskFactory
    {
        public static IProcessorTaskFactory Instance
        {
            get { return LazySingletonFactory<ProcessorLongTaskFactory>.Instance; }
        }

        public ProcessorTask NewTask
        {
            get
            {
                var task = SimpleProcessorTaskFactory.Instance.NewTask;
                Log.WriteLine("Prolonging new task...");
                task.RuntimeSeconds *= 2.0f;
                return task;
            }
        }
    }
}
