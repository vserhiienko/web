﻿namespace Labwork02.IndividualTask
{
    public class ProcessorShortTaskFactory : IProcessorTaskFactory
    {
        public static IProcessorTaskFactory Instance
        {
            get { return LazySingletonFactory<ProcessorShortTaskFactory>.Instance; }
        }

        public ProcessorTask NewTask
        {
            get
            {
                var task = SimpleProcessorTaskFactory.Instance.NewTask;
                Log.WriteLine("Shortening new task...");
                task.RuntimeSeconds /= 2.0f;
                return task;
            }
        }
    }
}
