﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labwork02.IndividualTask
{
    public class ProcessorCachableTaskFactory : ProcessorTaskCache, IProcessorTaskFactory
    {
        public static IProcessorTaskFactory Instance
        {
            get { return LazySingletonFactory<ProcessorCachableTaskFactory>.Instance; }
        }

        public ProcessorCachableTaskFactory()
        {
        }

        public ProcessorTask NewTask
        {
            get
            {
                return PullTask(ProcessorTask.RandomRuntimeSeconds);
            }
        }

    }
}
