﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Labwork02.IndividualTask
{
    class ProcessorTaskJsonDeserializer
    {
        static JavaScriptSerializer jsonSerializer;

        static ProcessorTaskJsonDeserializer()
        {
            jsonSerializer = new JavaScriptSerializer();
        }

        public ProcessorTask Serialize(string task)
        {
            return jsonSerializer.Deserialize<ProcessorTask>(task);
        }
    }
}
