﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labwork02.IndividualTask
{
    public class Log
    {
        private static readonly object syncObject = new object();
        public static void WriteLine(string format, params object[] args)
        {
            lock (syncObject)
            {
                Debug.WriteLine(string.Format(format, args));
            }
        }
        public static void Write(string format, params object[] args)
        {
            lock (syncObject)
            {
                Debug.Write(string.Format(format, args));
            }
        }
    }
}
