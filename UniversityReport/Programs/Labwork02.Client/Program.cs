﻿using Labwork02.IndividualTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Labwork02.Client
{
    class Program
    {
        static Program Instance
        {
            get
            {
                return IndividualTask.LazySingletonFactory<Program>.Instance;
            }
        }


        private byte[] receivedBytes = new byte[1024];
        private Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        void LoopConnect()
        {
            int attemptCount = 0;

            while(!clientSocket.Connected)
            {
                try
                {
                    ++attemptCount;
                    clientSocket.Connect(IPAddress.Loopback, 100);
                }
                catch (SocketException)
                {
                    Console.Clear();
                    Console.WriteLine("Connection attempts: " + attemptCount);
                }
            }

            Console.Clear();
            Console.WriteLine("Connected");
        }

        private void SetExitCallback()
        {
            AppDomain.CurrentDomain.ProcessExit += OnProcessExit;
            AppDomain.CurrentDomain.DomainUnload += OnProcessExit;
            Console.CancelKeyPress += OnConsoleCancelled;
        }

        private void OnConsoleCancelled(object sender, ConsoleCancelEventArgs e)
        {
            Instance.SendGoodBye();
        }

        static void Main(string[] args)
        {
            Console.Title = "VladSerhiienko, Labwork02: Connecting to server...";

            var program = Instance;
            program.LoopConnect();
            program.SetExitCallback();

            if (args.Length > 0 && args[0] == "-tasks")
                program.LoopSendTasks();
            else
                program.LoopSend();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static void OnProcessExit(object sender, EventArgs e)
        {
            Instance.SendGoodBye();
        }

        private void SetClientName()
        {
            var requestString = "-get-name";
            byte[] requestBytes = Encoding.ASCII.GetBytes(requestString);

            try
            {
                clientSocket.Send(requestBytes);
                clientSocket.Receive(receivedBytes);
                var responseString = Encoding.ASCII.GetString(receivedBytes).Trim();
                Console.Title = "VladSerhiienko, Labwork02: " + responseString + " (Client)";
            }
            catch (SocketException e)
            {
                Console.WriteLine("Error occured, when trying to send/received data: " + e.Message);
            }
            finally
            {
                Array.Clear(receivedBytes, 0, receivedBytes.Length);
            }
        }

        private void SendGoodBye()
        {
            if (clientSocket == null) return;

            var requestString = GoodbyeMessage;
            var requestBytes = Encoding.ASCII.GetBytes(requestString);

            try
            {
                clientSocket.Send(requestBytes);
                clientSocket.Disconnect(true);
                clientSocket.Dispose();
                clientSocket = null;
            }
            catch (SocketException e)
            {
                Console.WriteLine("Error occured, when trying to send/received data: " + e.Message);
            }
            finally
            {
                Array.Clear(receivedBytes, 0, receivedBytes.Length);
            }
        }

        private bool GoodbyeHandled(string userInput)
        {
            if (userInput != GoodbyeMessage) return false;
            SendGoodBye();
            return true;
        }

        const string GoodbyeMessage = "-bye";

        private void LoopSend()
        {
            SetClientName();

            while (true)
            {
                Console.Write("Enter a request: ");
                var requestString = Console.ReadLine();
                if (GoodbyeHandled(requestString))
                    break;

                var responseString = string.Empty;
                byte[] requestBytes = Encoding.ASCII.GetBytes(requestString);

                try
                {
                    clientSocket.Send(requestBytes);
                    clientSocket.Receive(receivedBytes);
                    responseString = Encoding.ASCII.GetString(receivedBytes, 0, Array.IndexOf(receivedBytes, (byte)0)).Trim();
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Error occured, when trying to send/received data: " + e.Message);
                    break;
                }
                finally
                {
                    Array.Clear(receivedBytes, 0, receivedBytes.Length);
                }

                Console.WriteLine("Received message: " + responseString);
            }

            SendGoodBye();
        }

        private void LoopSendTasks()
        {
            SetClientName();

            IProcessorTaskFactory taskFactory = ProcessorCachableTaskFactory.Instance;

            while (true)
            {
                var task = taskFactory.NewTask;

                Thread.Sleep(TimeSpan.FromSeconds(task.RuntimeSeconds));
                var requestString = "-task:" + task.RuntimeSeconds.ToString();

                var responseString = string.Empty;
                byte[] requestBytes = Encoding.ASCII.GetBytes(requestString);

                try
                {
                    clientSocket.Send(requestBytes);
                    clientSocket.Receive(receivedBytes);
                    responseString = Encoding.ASCII.GetString(receivedBytes, 0, Array.IndexOf(receivedBytes, (byte)0)).Trim();
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Error occured, when trying to send/received data: " + e.Message);
                    break;
                }
                finally
                {
                    Array.Clear(receivedBytes, 0, receivedBytes.Length);
                }

                Console.WriteLine("Received message: " + responseString);
            }

            SendGoodBye();
        }
    }
}
